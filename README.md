# LeasePlan automation project

## Table of contents

* [Technologies](#technologies)
* [Folder structure conventions](#The project directory structure)
* [The sample scenario](#The sample scenario)
* [The Action Classes implementation.](#The Action Classes implementation.)
* [Executing the tests](#Executing the tests)
* [Generating the reports](#Generating the reports)
* [Refactored parts](#Refactoring the framework)
* [Want to learn more?](#Want to learn more?)

## Technologies

- REST ASSURED - Java DSL for testing of REST based services
- CUCUMBER - software tool used for acceptance tests written in a behavior-driven development style
- LOMBOK 1.18.6 - Java library helps to avoid repetitive code
- Slf4j - Java logging framework

Mac OS installation instructions:
* Install Java JDK 8 on your local machine
* Install Maven
    ```
    $ brew install maven
    ```
* Enable in Intellij the `Annotation processors` settings


## The project directory structure

The project has build scripts for both Maven and Gradle, and follows the standard directory
structure used in most Serenity projects:

```Gherkin
src
+ main
+ test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
     + search                  Feature file subdirectories 
             search_product.feature
```


## The sample scenario
Both variations of the sample project uses the sample Cucumber scenario. In this scenario, Sergey (who likes to search for stuff) is performing a search on the internet:

```Gherkin
  Scenario Outline: Validate that we receive the corresponding responses when we are searching for a product
    When he is searching for available <productName>
    Then the response code is 200
    And he does not see any errors in the results
    And all results title contains product name <productName>
    Examples:
      | productName |
      | apple       |
      | mango       |
      | tofu        |
      | water       |
```


### The Action Classes implementation.

A more imperative-style implementation using the Action Classes pattern can be found in the `action-classes` branch. The glue code in this version looks this this:

```java
    @Given("^(?:.*) is researching things on the internet")
    public void i_am_on_the_Wikipedia_home_page() {
        navigateTo.theHomePage();
    }

    @When("she/he looks up {string}")
    public void i_search_for(String term) {
        searchFor.term(term);
    }

    @Then("she/he should see information about {string}")
    public void all_the_result_titles_should_contain_the_word(String term) {
        assertThat(searchResult.displayed()).contains(term);
    }
```

These classes are declared using the Serenity `@Steps` annotation, shown below:
```java
    @Steps
    NavigateTo navigateTo;

    @Steps
    SearchFor searchFor;

    @Steps
    SearchResult searchResult;
```

The `@Steps`annotation tells Serenity to create a new instance of the class, and inject any other steps or page objects that this instance might need.

Each action class models a particular facet of user behaviour: navigating to a particular page, performing a search, or retrieving the results of a search. These classes are designed to be small and self-contained, which makes them more stable and easier to maintain.

The `NavigateTo` class is an example of a very simple action class. In a larger application, it might have some other methods related to high level navigation, but in our sample project, it just needs to open the DuckDuckGo home page:
```java
public class NavigateTo {

    WikipediaHomePage homePage;

    @Step("Open the Wikipedia home page")
    public void theHomePage() {
        homePage.open();
    }
}
```

## Executing the tests
To run the sample project, you can either just run the `CucumberTestSuite` test runner class, or run either `mvn verify` or `gradle test` from the command line.

```json
$ mvn clean verify serenity:aggregate
```

The test results will be recorded in the `target/site/serenity` directory.

## Generating the reports
Since the Serenity reports contain aggregate information about all of the tests, they are not generated after each individual test (as this would be extremenly inefficient). Rather, The Full Serenity reports are generated by the `serenity-maven-plugin`. You can trigger this by running `mvn serenity:aggregate` from the command line or from your IDE.

They reports are also integrated into the Maven build process: the following code in the `pom.xml` file causes the reports to be generated automatically once all the tests have completed when you run `mvn verify`?

```
             <plugin>
                <groupId>net.serenity-bdd.maven.plugins</groupId>
                <artifactId>serenity-maven-plugin</artifactId>
                <version>${serenity.maven.version}</version>
                <configuration>
                    <tags>${tags}</tags>
                </configuration>
                <executions>
                    <execution>
                        <id>serenity-reports</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
```
- Reports are generated on Gitlab for each build 
- The report can be found [here](https://gitlab.com/bojtetomi92/leaseplan-automation/-/jobs)
- Each run collect the report and save it as artifact
- To access the report you need to open the job run, click `Browse` and after follow the route `target/site/serenity/index.html`

## Refactoring the framework

1. Refactored the cucumber tests using `Scenario Outline` to reduce the code and introducing the
   product names as variables
2. Using of implementation and steps to have specific behavior and extract the logic from steps
3. Removed everything related to webdriver, because it is not used
4. Removed the unused classes and configurations
5. Used lambda expressions to not have any spaghetti code
6. Used lombok for DTO and @Getter and @Setter to reduce the code writing

## Want to learn more?
* [Java Tutorial](https://www.w3schools.com/java/)
* [Rest Assured](https://github.com/rest-assured/rest-assured/wiki/Usage)
* [Cucumber Documentation](https://cucumber.io/docs/guides/)
* [Lombok](https://projectlombok.org/)
* [JUnit Documentation](https://junit.org/junit5/docs/current/user-guide/)
* [What is REST API?](https://www.quora.com/What-is-a-REST-API)
