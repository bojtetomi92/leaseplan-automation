package com.leaseplan.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

  @JsonProperty("image")
  private String image;

  @JsonProperty("unit")
  private String unit;

  @JsonProperty("provider")
  private String provider;

  @JsonProperty("price")
  private Double price;

  @JsonProperty("title")
  private String title;

  @JsonProperty("promoDetails")
  private String promoDetails;

  @JsonProperty("brand")
  private String brand;

  @JsonProperty("isPromo")
  private Boolean isPromo;

  @JsonProperty("url")
  private String url;
}
