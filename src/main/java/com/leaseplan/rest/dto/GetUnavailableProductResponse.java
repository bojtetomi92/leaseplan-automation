package com.leaseplan.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GetUnavailableProductResponse {

  @JsonProperty("detail")
  private Detail detail;
}
