package com.leaseplan.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Detail {

  @JsonProperty("served_by")
  private String servedBy;

  @JsonProperty("error")
  private Boolean error;

  @JsonProperty("message")
  private String message;

  @JsonProperty("requested_item")
  private String requestedItem;
}
