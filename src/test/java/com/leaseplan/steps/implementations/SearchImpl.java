package com.leaseplan.steps.implementations;

import static net.serenitybdd.rest.SerenityRest.rest;

import com.leaseplan.rest.World;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SearchImpl {

  public static final String WEBSITE = "https://waarkoop-server.herokuapp.com/api/v1/search/test/";

  private RequestSpecification searchSpec() {

    return rest()
        .baseUri(WEBSITE)
        .contentType(ContentType.JSON)
        .log()
        .all()
        .when();
  }

  public void getAvailableProductsByKeyWord(String productName) {
    log.info("Get product by name {}", productName);
    World.setResponse(searchSpec().get(productName));
  }
}
