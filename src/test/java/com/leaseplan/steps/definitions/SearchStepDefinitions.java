package com.leaseplan.steps.definitions;

import com.leaseplan.rest.World;
import com.leaseplan.rest.dto.GetUnavailableProductResponse;
import com.leaseplan.rest.dto.Product;
import com.leaseplan.steps.implementations.SearchImpl;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

public class SearchStepDefinitions {

  @Steps public SearchImpl searchImpl;

  @When("he is searching for available {}")
  public void heIsSearchingForProduct(String productName) {
    searchImpl.getAvailableProductsByKeyWord(productName);
  }

  @Then("all results title contains product name {}")
  public void allResultsContainsTheProductName(String productName) {
    List<Product> productsList =
        Arrays.stream(World.getResponse().body().as(Product[].class)).collect(Collectors.toList());

    productsList.forEach(
        product -> Assertions.assertThat(product.getTitle()).containsIgnoringCase(productName));
  }

  @Then("the response code is {int}")
  public void theResponseCodeIs(int responseCode) {
    Assertions.assertThat(World.getResponse().statusCode()).isEqualTo(responseCode);
  }

  @And("he does not see any errors in the results")
  public void heDoesNotSeeAnyErrorsInTheResults() {
    Assertions.assertThat(World.getResponse().getBody().prettyPrint())
        .doesNotContainIgnoringCase("error");
  }

  @And("he can see errors in the results")
  public void heCanSeeErrorsInTheResults() {
    GetUnavailableProductResponse detail =
        World.getResponse().body().as(GetUnavailableProductResponse.class);
    Assertions.assertThat(detail.getDetail().getError()).isTrue();
  }

  @And("all results has the message {}")
  public void allResultsHasTheMessage(String message) {
    GetUnavailableProductResponse detail =
        World.getResponse().body().as(GetUnavailableProductResponse.class);
    Assertions.assertThat(detail.getDetail().getMessage()).isEqualTo(message);
  }
}
