@Regression
Feature: Search for the product

  @Positive-Test
  Scenario Outline: Validate that we receive the corresponding responses when we are searching for a product
    When he is searching for available <productName>
    Then the response code is 200
    And he does not see any errors in the results
    And all results title contains product name <productName>
    Examples:
      | productName |
      | apple       |
      | mango       |
      | tofu        |
      | water       |

  @Negative-Test
  Scenario Outline: Searching for a non existent product(including the ignore case checking)
    When he is searching for available <productName>
    Then the response code is 404
    And he can see errors in the results
    And all results has the message Not found
    Examples:
      | productName   |
      | Apricots      |
      | Apple         |
      | Avocado       |
      | Banana        |
      | Blackberries  |
      | Blackcurrant  |
      | Blueberries   |
      | Breadfruit    |
      | Cantaloupe    |
      | Carambola     |
      | Cherimoya     |
      | Cherries      |
      | Clementine    |
      | Cranberries   |
      | Durian        |
      | Elderberries  |
      | Feijoa        |
      | Figs          |
      | Gooseberries  |
      | Grapefruit    |
      | Grapes        |
      | Guava         |
      | Jackfruit     |
      | Jujube        |
      | Kiwifruit     |
      | Kumquat       |
      | Lemon         |
      | lime          |
      | Lime          |
      | Longan        |
      | Loquat        |
      | Lychee        |
      | Mandarin      |
      | Mango         |
      | Mangosteen    |
      | Mulberries    |
      | Nectarine     |
      | Olives        |
      | Orange        |
      | Papaya        |
      | Passion-Fruit |
      | Peaches       |
      | Pear          |
      | Persimmon     |
      | Pitaya        |
      | Pineapple     |
      | Pitanga       |
      | Plantain      |
      | Plums         |
      | Pomegranate   |
      | Prickly  Pear |
      | Prunes        |
      | Pummelo       |
      | Quince        |
      | Raspberries   |
      | Rhubarb       |
      | Rose-Apple    |
      | Sapodilla     |
      | Sapote        |
      | Soursop       |
      | Strawberries  |
      | Sugar-Apple   |
      | Tamarind      |
      | Tangerine     |
      | Watermelon    |

  @Negative-Test
  Scenario Outline: Searching for a product with sql injection
    When he is searching for available <productName>
    Then the response code is 404
    And he can see errors in the results
    And all results has the message Not found
    Examples:
      | productName        |
      | Apricots'+OR+1=1-- |
      | Apricots'--        |

  @Negative-Test
  Scenario Outline: Searching for multiple products
    When he is searching for available <productsName>
    Then the response code is 404
    And he can see errors in the results
    And all results has the message Not found
    Examples:
      | productsName |
      | apple,mango  |
      | apple&mango  |
      | apple;mango  |